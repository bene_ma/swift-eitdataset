import XCTest
import Foundation

import Datasets

@testable import EITDataset

final class EITDatasetTests: XCTestCase {
    struct UnexpectedNilError: Error {}
    
    let localStorageDirectory = DatasetUtilities.defaultDirectory.appendingPathComponent("EITDataset", isDirectory: true)
    
    func test1Download() {
        EITDatasetUtilities.downloadAndUnzip(from: EITDatasetUtilities.defaultArchiveURL, to: localStorageDirectory, unzip: true, cache: true)
    }
    
    func test2MaskParsing() {
        
        let maskArray = EITDatasetUtilities.loadImageMask(from: localStorageDirectory.appendingPathComponent("mask.mat"))
        
        XCTAssertEqual(maskArray.count, 4096)
    }
    
    func test3DataParsing() {
        let eitData = EITDatasetUtilities.loadEITDatasetFile(from: localStorageDirectory.appendingPathComponent("validation/circle-1_99.mat"))
        if let eitData = eitData {
            XCTAssertEqual(eitData.count, 99)
            
            // there may be only finite values in the data
            XCTAssertEqual(eitData.filter({ $0.voltages.filter({ $0.isFinite }).count == 0 }).count, 0)
            XCTAssertEqual(eitData.filter({ $0.conductivity.filter({ $0.isFinite }).count == 0 }).count, 0)
        } else {
            XCTFail("Error while unwrapping optional")
        }
        
    }
    
    func test4LoadingDataset() throws {
        let dataset = EITDataset(batchSize: 32)
        XCTAssertNotNil(dataset);
        
        guard let data = dataset.validation.first else { throw UnexpectedNilError() }
        
        XCTAssertEqual(data.data.shape, [32, 16, 13, 1])
        XCTAssertEqual(data.label.shape, [32, 64, 64, 1])
        
        
        XCTAssertEqual(dataset.nanMask.shape, [64, 64, 1])
        
        let image = dataset.getImage(from: data.label[0])
        XCTAssertEqual(image.count, 64)
        XCTAssertEqual(image[0].count, 64)
        XCTAssertEqual(image[0][0].count, 3)
    }

    static var allTests = [
        ("test1Download", test1Download),
        ("test2MaskParsing", test2MaskParsing),
        ("test3DataParsing", test3DataParsing),
        ("test4LoadingDataset", test4LoadingDataset),
    ]
}

import XCTest

import swift_eitdatasetTests

var tests = [XCTestCaseEntry]()
tests += swift_eitdatasetTests.allTests()
XCTMain(tests)

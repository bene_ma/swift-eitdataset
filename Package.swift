// swift-tools-version:5.3

import PackageDescription

let package = Package(
    name: "swift-eitdataset",
    platforms: [
        .macOS(.v10_13)
    ],
    products: [
        .library(
            name: "EITDataset", targets: ["EITDataset"]),
    ],
    dependencies: [
        .package(url: "https://github.com/pvieito/PythonKit.git", .branch("master")),
        .package(url: "https://github.com/tensorflow/swift-models", .branch("main")),
    ],
    targets: [
        .target(
            name: "EITDataset",
            dependencies: [
                .product(name: "Datasets", package: "swift-models"),
                .product(name: "ModelSupport", package: "swift-models"),
            ]),
        .testTarget(
            name: "EITDatasetTests",
            dependencies: ["EITDataset"]),
    ]
)

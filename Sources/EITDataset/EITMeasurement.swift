//
//  helpers.swift
//
//
//  Created by Benedikt Eliasson on 27.01.21.
//
// A simple struct holding voltage measurements and conductivity data

public struct EITMeasurement {
    public let voltages: [Float]
    public let conductivity: [Float]
    
    public static let measurementSize: Int = 208
    public static let imageSize: Int = 64
}


//
//  helpers.swift
//
//
//  Created by Benedikt Eliasson on 27.01.21.
//
// A Dataset implementation for EIT data

import Foundation
import TensorFlow

import ModelSupport
import Datasets

// A batch of electrode measurement with a conductivity distribution
// (usually <batchSize, 16, 13, 1>,<batchSize, 64, 64, 1>)
public typealias LabeledEITData = LabeledData<Tensor<Float>, Tensor<Float>>

// We define a EITClassificationData type, whose elements represent LabeledEITData
public protocol EITClassificationData {
    // Training is a sequence of epochs, each epochs is a collection of batches containing a LabeledEITData
    associatedtype Training: Sequence
    where Training.Element: Collection, Training.Element.Element == LabeledEITData;
    // Validation is a collection of batches containing a LabeledEITData
    associatedtype Validation: Collection
    where Validation.Element == LabeledEITData;
    
    /// Creates an instance from a given `batchSize`.
    init(batchSize: Int, on device: Device)
    
    /// The `training` epochs.
    var training: Training { get }
    /// The `validation` batches.
    var validation: Validation { get }
}

public struct EITDataset<Entropy: RandomNumberGenerator> {
    /// Type of the collection of non-collated batches.
    public typealias Batches = Slices<Sampling<[EITMeasurement], ArraySlice<Int>>>
    /// The type of the training data, represented as a sequence of epochs, which
    /// are collection of batches.
    public typealias Training = LazyMapSequence<
        TrainingEpochs<[EITMeasurement], Entropy>,
        LazyMapSequence<Batches, LabeledEITData>
    >
    /// The type of the validation data, represented as a collection of batches.
    public typealias Validation = LazyMapSequence<Slices<[EITMeasurement]>, LabeledEITData>
    /// The training epochs.
    public let training: Training
    /// The validation batches.
    public let validation: Validation
    
    public let nanMask: Tensor<Bool>

    /// Creates an instance with `batchSize` on `device` using `remoteArchiveLocation`.
    /// - Parameters:
    ///   - batchSize: the size of batches, that the data will be chunked into
    ///   - entropy: a source of randomness used to shuffle sample ordering.  It
    ///     will be stored in `self`, so if it is only pseudorandom and has value
    ///     semantics, the sequence of epochs is deterministic and not dependent
    ///     on other operations.
    ///   - device: a TensorFlow `Device` to store the dataset on
    ///   - remoteArchiveLocation: an `URL` to the remote location of the dataset
    ///   - localStorageDirectory: an `URL` to the local directory to store the dataset in
    public init(
        batchSize: Int,
        entropy: Entropy,
        device: Device,
        remoteArchiveLocation: URL = EITDatasetUtilities.defaultArchiveURL,
        localStorageDirectory: URL = EITDatasetUtilities.defaultStorageDirectory
    ) {
        EITDatasetUtilities.downloadAndUnzip(from: remoteArchiveLocation, to: localStorageDirectory)
        
        // read and parse training data
        let trainingSamples = EITDatasetUtilities.loadEITDatasetFiles(in: localStorageDirectory.appendingPathComponent("training"))
        print("Loaded \(trainingSamples.count) training samples")
        training = TrainingEpochs(samples: trainingSamples, batchSize: batchSize, entropy: entropy)
            .lazy.map { (batches: Batches) -> LazyMapSequence<Batches, LabeledEITData> in
                return batches.lazy.map{
                    makeBatch(samples: $0, device: device)
                }
            }
        
        // read and parse validation data
        let validationSamples = EITDatasetUtilities.loadEITDatasetFiles(in: localStorageDirectory.appendingPathComponent("validation"))
        print("Loaded \(validationSamples.count) validation samples")
        validation = validationSamples.inBatches(of: batchSize).lazy.map {
            return makeBatch(samples: $0, device: device)
        }
        
        let mask = EITDatasetUtilities.loadImageMask(from: localStorageDirectory.appendingPathComponent("mask.mat"))
        nanMask = Tensor<Bool>(shape: TensorShape([EITMeasurement.imageSize, EITMeasurement.imageSize, 1]), scalars: mask, on: device)
    }
    
    /// Creates an RGB image from a conductivity distribution. A min and max for scaling the RGB image can be passed in separately.
    /// Also a custom colormapping can be passed. The default color map is matplotlib's default 'viridis' colormap
    /// - Parameters:
    ///   - conductivity: a `Tensor<Float>` of conductivity distribution. the Shape needs to be [64, 64, 1]
    ///   - min: the min threshold used for scaling the conductivity to a range of 0-255 (values outside will be clamped)
    ///   - max: the min threshold used for scaling the conductivity to a range of 0-255 (values outside will be clamped)
    ///   - colormap: a color mapping for the input. the scaled value (range: 0-255) will be used as an index and the value of the color map
    ///             will be returned. The colormap uses Float values which range between 0 and 1. Those values will be scaled to the RGB range
    public func getImage(from conductivity: Tensor<Float>,
                         min: Float = -4,
                         max: Float = 0,
                         colormap: [[Float]] = EITDatasetUtilities.viridisColormap
                        ) -> [[[UInt8]]] {
        precondition(conductivity.shape == [64, 64, 1], "The shape of the conductivity Tensor needs to be [64, 64, 1]")
        precondition(colormap.count == 256, "The colormap array needs to have 256 entries")
        precondition(!colormap.contains(where: { $0.count != 3}), "each colormap entry needs to have 3 values for RGB")
        precondition(!colormap.contains(where: { rgb in rgb.contains(where: { $0 < 0.0 || $0 > 1.0 }) }), "the colormap entries may only range from 0-1")
    
        let maskedConductivity = log10(conductivity).replacing(with: Tensor<Float>(repeating: Float.nan, shape: conductivity.shape, on: conductivity.device), where: self.nanMask)
        return EITDatasetUtilities.RGBImage(from: maskedConductivity.scalars, min: min, max: max, colormap: colormap)
    }
}

extension EITDataset: EITClassificationData where Entropy == SystemRandomNumberGenerator {
    /// Wrapper init. Creates an instance with `batchSize`. on a `Device`
    public init(batchSize: Int, on device: Device = Device.default) {
        self.init(batchSize: batchSize, entropy: SystemRandomNumberGenerator(), device: device)
    }
    
    /// Wrapper init. Creates an instance with `batchSize`. on a `Device` using a given `remoteArchiveLocation` and `localStorageDirectory`
    public init(batchSize: Int,
                on device: Device = Device.default,
                remoteArchiveLocation: URL = EITDatasetUtilities.defaultArchiveURL,
                localStorageDirectory: URL = DatasetUtilities.defaultDirectory) {
        self.init(batchSize: batchSize,
                  entropy: SystemRandomNumberGenerator(),
                  device: device,
                  remoteArchiveLocation: remoteArchiveLocation,
                  localStorageDirectory: localStorageDirectory)
    }

}


/// Convert samples of type (`EITMeasurement`) into batches with correct dimensions
private func makeBatch<BatchSamples: Collection>(
    samples: BatchSamples, device: Device
) -> LabeledEITData where BatchSamples.Element == (EITMeasurement) {
    // Split the `EITMeasurement` array into separate voltages and conductivity arrays
    let voltageBytes = samples.lazy.map(\.voltages).reduce(into: [], +=)
    let conductivityBytes = samples.lazy.map(\.conductivity).reduce(into: [], +=)
    
    // create a `Tensor` from the arrays, copy them onto the device
    let voltages = Tensor<Float>(
        shape: [samples.count, 16, 13, 1],
        scalars: voltageBytes,
        on: device)
    let conductivities = Tensor<Float>(
        shape: [samples.count, EITMeasurement.imageSize, EITMeasurement.imageSize, 1],
        scalars: conductivityBytes,
        on: device)
    
    return LabeledEITData(data: voltages, label: conductivities)
}
